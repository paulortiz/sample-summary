//
//  SubHeaderView.swift
//  BankSummary
//
//  Created by Paul Ortiz on 4/29/18.
//  Copyright © 2018 Paul Ortiz. All rights reserved.
//

import UIKit

class SubHeaderView: UIView {
    
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var greetingsLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupViews()
    }
    
    func setupViews() {
        self.backgroundColor = .red
    }
}
