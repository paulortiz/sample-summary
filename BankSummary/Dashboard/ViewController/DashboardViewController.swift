//
//  DashboardViewController.swift
//  BankSummary
//
//  Created by Paul Ortiz on 4/28/18.
//  Copyright © 2018 Paul Ortiz. All rights reserved.
//

import UIKit

class DashboardViewController: UIViewController {
    
    var accountSummaryView: AccountSummaryView!
    var assetBankSummary: BankSummary!
    var liabilityBankSummary: BankSummary!
    var bankSummaryList: [BankSummary]!
    
    @IBOutlet weak var welcomeLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        commonInit()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        commonInit()
    }
    
    func commonInit() {
        setupNavigationController()
        setupTableView()
        loadData()
    }
    
    fileprivate func loadData() {
        assetBankSummary = Mock.loadAssets()
        liabilityBankSummary = Mock.loadLiabilities()
        bankSummaryList = [assetBankSummary, liabilityBankSummary]
        accountSummaryView.updateData(bankSummaryList: bankSummaryList)
    }
    
    fileprivate func setupTableView() {
        accountSummaryView = AccountSummaryView()
        
        if let tableView = accountSummaryView {
            tableView.backgroundColor = .clear
            tableView.showsVerticalScrollIndicator = false
            tableView.showsHorizontalScrollIndicator = false
            tableView.contentInset = UIEdgeInsets(top: 180, left: 0, bottom: 0, right: 0)
            tableView.contentOffset = CGPoint(x: 0, y: -180)
            tableView.rowHeight = UITableViewAutomaticDimension
            tableView.tableFooterView = UIView()
            tableView.summaryViewDelegate = self
            view!.addSubview(accountSummaryView)
            view!.addConstraintWithFormat(format: "H:|[v0]|", views: tableView)
            view!.addConstraintWithFormat(format: "V:|[v0]|", views: tableView)
        }
        
    }
    
    fileprivate func setupNavigationController() {
        if let navigationController = self.navigationController {
            let navigationBar = navigationController.navigationBar
            navigationBar.setBackgroundImage(UIImage(), for: .default)
            navigationBar.tintColor = .white
            navigationBar.shadowImage = UIImage()
        }
    }
    
}

extension DashboardViewController : AccountSummaryViewDelegate {
    func accountSummaryView(accountSummaryView: AccountSummaryView, didScroll atPoint: CGPoint) {
        var offset = atPoint.y / 150
        if offset > -0.9 {
            offset = offset + 1
            let color = UIColor(red: 1, green: 1, blue: 1, alpha: offset)
            self.navigationController?.navigationBar.tintColor = UIColor(hue: 1, saturation: offset, brightness: 1, alpha: 1)
            self.navigationController?.navigationBar.backgroundColor = color
            UIApplication.shared.statusBarView?.backgroundColor = color
            welcomeLabel.alpha = offset
        } else {
            let color = UIColor(red: 1, green: 1, blue: 1, alpha: offset)
            self.navigationController?.navigationBar.tintColor = UIColor(hue: 1, saturation: offset, brightness: 1, alpha: 1)
            self.navigationController?.navigationBar.backgroundColor = color
            UIApplication.shared.statusBarView?.backgroundColor = color
            welcomeLabel.alpha = offset 
        }
    }
}
