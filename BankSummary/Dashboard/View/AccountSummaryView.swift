//
//  AccountSummaryTableView.swift
//  BankSummary
//
//  Created by Paul Ortiz on 4/28/18.
//  Copyright © 2018 Paul Ortiz. All rights reserved.
//

import UIKit

protocol AccountSummaryViewDelegate {
    func accountSummaryView(accountSummaryView: AccountSummaryView, didScroll atPoint: CGPoint)
}

class AccountSummaryView: UITableView {
    
    var headerView: UIView!
    var tableView: UITableView!
    var screenHeight: CGFloat!
    var screenWidth: CGFloat!
    var summaryViewDelegate: AccountSummaryViewDelegate?
    var data: [BankSummary]?
    var bankSummaryList: [BankSummary]?
    var reuseableId: String = "account_summary_cell"
    var summaryDetails: String = "account_summary_details_cell"
    var selectedIndexPath: IndexPath?
    
    override init(frame: CGRect, style: UITableViewStyle) {
        super.init(frame: frame, style: .grouped)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupViews()
    }
    
    func setupViews() {
        delegate = self
        dataSource = self
        estimatedRowHeight = 44
        rowHeight = UITableViewAutomaticDimension
        
        let accountSummaryViewCell = UINib(nibName: "AccountSummaryViewCell", bundle: nil)
        let accountSummarListViewCell = UINib(nibName: "AccountSummaryListViewCell", bundle: nil)
        
        register(accountSummaryViewCell, forCellReuseIdentifier: reuseableId)
        register(accountSummarListViewCell, forCellReuseIdentifier: summaryDetails)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let position = scrollView.contentOffset
        summaryViewDelegate?.accountSummaryView(accountSummaryView: self, didScroll: position)
    }
    
    func updateData(bankSummaryList: [BankSummary]) {
        self.bankSummaryList = bankSummaryList
        self.data = bankSummaryList
        self.reloadData()
    }
}

extension AccountSummaryView : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return (bankSummaryList != nil) ? bankSummaryList!.count : 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = SectionHeader()
        view.titleLabel.text = bankSummaryList?[section].category
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var openedIndex: Int = 0
        var count: Int = 0
        
        if let indexPath = selectedIndexPath {
            if let summary = bankSummaryList?[section].summmaryList?[indexPath.row], let accounts = summary.accounts {
                openedIndex = summary.isExpanded ? accounts.count : 0
            }
        }
        
        if let accountSummary = bankSummaryList?[section].summmaryList {
            count = accountSummary.count + openedIndex
        }
        
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let section = indexPath.section
        let row = indexPath.row
        let accountSummary = bankSummaryList?[section].summmaryList?[row]
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: reuseableId, for: indexPath) as? AccountSummaryViewCell {
            
            cell.productTypeLabel.text = accountSummary?.prodDesc
            cell.amountLabel.text = accountSummary?.totalBalance
            cell.currencyLabel.text = accountSummary?.currency
            
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "empty", for: indexPath)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let section = indexPath.section
        let row = indexPath.row
        
        if let summary = bankSummaryList?[section].summmaryList?[row] {
            let isExpanded = summary.isExpanded
            print("is expanded ? \(isExpanded)")
            selectedIndexPath = indexPath
            bankSummaryList?[section].summmaryList?[row].isExpanded = !isExpanded
            //self.beginUpdates()
            //self.insertRows(at: [IndexPath(row: indexPath.row + 1, section: indexPath.section)], with: .top)
            //self.endUpdates()
        }
    }
}
