//
//  SectionHeader.swift
//  BankSummary
//
//  Created by Paul Ortiz on 5/3/18.
//  Copyright © 2018 Paul Ortiz. All rights reserved.
//

import UIKit

class SectionHeader: UIView {
    
    var titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 18)
        label.textColor = .gray
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupViews()
    }
    
    func setupViews() {
        backgroundColor = UIColor(hex: "#F0F0F0")
        addSubview(titleLabel)
        addConstraintWithFormat(format: "H:|-14-[v0]", views: titleLabel)
        addConstraintWithFormat(format: "V:|[v0]|", views: titleLabel)
    }
}
