//
//  AccountSummaryListViewCell.swift
//  BankSummary
//
//  Created by Paul Ortiz on 5/3/18.
//  Copyright © 2018 Paul Ortiz. All rights reserved.
//

import UIKit

class AccountSummaryListViewCell: UITableViewCell {

    @IBOutlet weak var accountCurrencyLabel: UILabel!
    @IBOutlet weak var accountBalanceLabel: UILabel!
    @IBOutlet weak var accountNameLabel: UILabel!
    @IBOutlet weak var accountNoLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
