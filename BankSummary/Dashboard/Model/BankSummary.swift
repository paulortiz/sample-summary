//
//  BankSummary.swift
//  BankSummary
//
//  Created by Paul Ortiz on 5/3/18.
//  Copyright © 2018 Paul Ortiz. All rights reserved.
//

import Foundation

class BankSummary {
    
    var category: String!
    var totalAvailableBalance: String!
    var currency: String!
    var summmaryList : [AccountSummary]? // this would contain both Asset and Liabilities
    
    init() {}
    
    class func parseJson(jsonDic: [String : AnyObject]) -> BankSummary {
        let bankSummary = BankSummary()
        
        if let category = jsonDic["category"] as? String {
            bankSummary.category = category
        }
        
        if let totalAvailableBalance = jsonDic["totalAvailableBalance"] as? String {
            bankSummary.totalAvailableBalance = totalAvailableBalance
        }
        
        if let currency = jsonDic["currency"] as? String {
            bankSummary.currency = currency
        }
        
        if let summaryListDic = jsonDic["summaryList"] as? [[String : AnyObject]] {
            bankSummary.summmaryList = AccountSummary.parseArray(jsonDicArray: summaryListDic)
        }
        
        return bankSummary
    }
}
