//
//  Mock.swift
//  BankSummary
//
//  Created by Paul Ortiz on 5/3/18.
//  Copyright © 2018 Paul Ortiz. All rights reserved.
//

import Foundation

class Mock {
    class func loadAssets() -> BankSummary {
        var bankSummary = BankSummary()
        if let path = Bundle.main.path(forResource: "assets", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                if let jsonDic = jsonResult as? [String : AnyObject] {
                    bankSummary = BankSummary.parseJson(jsonDic: jsonDic)
                }
            } catch {}
        }
        return bankSummary
    }
    
    class func loadLiabilities() -> BankSummary {
        var bankSummary = BankSummary()
        if let path = Bundle.main.path(forResource: "liabilities", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                if let jsonDic = jsonResult as? [String : AnyObject] {
                    bankSummary = BankSummary.parseJson(jsonDic: jsonDic)
                }
            } catch {}
        }
        return bankSummary
    }
}
