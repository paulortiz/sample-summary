//
//  Assets.swift
//  BankSummary
//
//  Created by Paul Ortiz on 5/3/18.
//  Copyright © 2018 Paul Ortiz. All rights reserved.
//

import Foundation
import UIKit

class AccountSummary {
    
    var prodDesc : String!
    var prodCode : String!
    var prodStatus : String!
    var totalBalance : String!
    var currency : String!
    var isExpanded: Bool = false
    var accounts : [Account]?
    
    init() {}
    
    class func parseArray(jsonDicArray: [[String : AnyObject]]) -> [AccountSummary] {
        var summaryList = [AccountSummary]()
        
        for jsonDic in jsonDicArray {
            summaryList.append(parse(jsonDic: jsonDic))
        }
        
        return summaryList
    }
    
    class func parse(jsonDic: [String : AnyObject]) -> AccountSummary {
        let accountSummary = AccountSummary()
        
        if let prodDesc = jsonDic["productDescription"] as? String {
            accountSummary.prodDesc = prodDesc
        }
        
        if let prodCode = jsonDic["prodCode"] as? String {
            accountSummary.prodCode = prodCode
        }
        
        if let prodStatus = jsonDic["prodStatus"] as? String {
            accountSummary.prodStatus = prodStatus
        }
        
        if let totalBalance = jsonDic["totalBalance"] as? String {
            accountSummary.totalBalance = totalBalance
        }
        
        if let currency = jsonDic["currency"] as? String {
            accountSummary.currency = currency
        }
        
        if let accountsDic = jsonDic["accounts"] as? [[String : AnyObject]] {
            accountSummary.accounts = Account.parseArray(jsonDicArray: accountsDic)
        }
        
        return accountSummary
    }
}
