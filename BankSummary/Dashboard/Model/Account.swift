//
//  Account.swift
//  BankSummary
//
//  Created by Paul Ortiz on 5/3/18.
//  Copyright © 2018 Paul Ortiz. All rights reserved.
//

import Foundation

class Account {
    var accountNo : String!
    var accountName: String!
    var balance : String!
    var currency : String!
    
    class func parseArray(jsonDicArray: [[String : AnyObject]]) -> [Account] {
        var accounts = [Account]()
        
        for jsonDic in jsonDicArray {
            accounts.append(parse(jsonDic: jsonDic))
        }
        
        return accounts
    }
    
    class func parse(jsonDic: [String : AnyObject]) -> Account {
        let account = Account()
        
        if let accountNo = jsonDic["accountNo"] as? String {
            account.accountNo = accountNo
        }
        
        if let accountName = jsonDic["accountName"] as? String {
            account.accountName = accountName
        }
        
        if let balance = jsonDic["balance"] as? String {
            account.balance = balance
        }
        
        if let currency = jsonDic["currency"] as? String {
            account.currency = currency
        }
        
        return account
    }
}
