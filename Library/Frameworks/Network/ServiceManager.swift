//
//  RequestManager.swift
//  BankSummary
//
//  Created by Paul Ortiz on 4/28/18.
//  Copyright © 2018 Paul Ortiz. All rights reserved.
//

import Foundation

class ServiceManager {
    static var request: NSMutableURLRequest?
    
    class func sendRequest<T: Codable>(service: String, model: T.Type, params: [String : Any]? = nil, method: String = "POST", completion: @escaping (T?) ->()) {
        guard let url = URL(string: BASE_URL + "/" + service) else {return}
        let session = URLSession.shared
        
        request = NSMutableURLRequest(url: url)
        request?.httpMethod = method
        request?.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request?.addValue("application/json", forHTTPHeaderField: "Accept")
        
        if let parameters = params {
            request?.httpBody = try! JSONSerialization.data(withJSONObject: parameters, options: [])
        }
        
        if let request = self.request as URLRequest? {
            session.dataTask(with: request) { data, response, error in
                guard let data = data else {return}
                do {
                    let object = try JSONDecoder().decode(model.self, from: data)
                    completion(object)
                } catch let error { print(error)}
            }.resume()
        }
    }
    
    class func sendRequest(service: String, params: [String : Any]? = nil, method: String = "POST", completion: @escaping (Data?, URLResponse?, Error?) ->()) {
        guard let url = URL(string: BASE_URL + "/" + service) else {return}
        let session = URLSession.shared
        
        request = NSMutableURLRequest(url: url)
        request?.httpMethod = method
        request?.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request?.addValue("application/json", forHTTPHeaderField: "Accept")
        
        if let parameters = params {
            request?.httpBody = try! JSONSerialization.data(withJSONObject: parameters, options: [])
        }
        
        if let request = self.request as URLRequest? {
            session.dataTask(with: request) { data, response, error in
                completion(data, response, error)
            }.resume()
        }
    }
}
