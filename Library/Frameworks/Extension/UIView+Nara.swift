//
//  UIView+Nara.swift
//  HomeFinder
//
//  Created by Paul Ortiz on 6/11/17.
//  Copyright © 2017 Paul Ortiz. All rights reserved.
//

import UIKit

extension UIView {
    func addConstraintWithFormat(format: String, views: UIView..., options: NSLayoutFormatOptions = NSLayoutFormatOptions()) {
        var viewsDictionary = [String : UIView]()
        
        for (index, view) in views.enumerated() {
            let key = "v\(index)";
            view.translatesAutoresizingMaskIntoConstraints = false
            viewsDictionary[key] = view
        }
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format, options: options, metrics: nil, views: viewsDictionary))
    }
    
    func addTopBorder(color: UIColor = UIColor(hex: "#979797"), borderWidth: CGFloat = 1) {
        let frame = CGRect(x: 0, y: 0, width: self.frame.size.width-1, height: borderWidth)
        self.addBorder(frame, color: color, borderWidth: borderWidth)
    }
    
    func addRightBorder(color: UIColor = UIColor(hex: "#979797"), borderWidth: CGFloat = 1) {
        let frame = CGRect(x: 0, y: 0, width: self.frame.size.width-1, height: borderWidth)
        self.addBorder(frame, color: color, borderWidth: borderWidth)
    }
    
    func addBottomBorder(color: UIColor = UIColor(hex: "#979797"), borderWidth: CGFloat = 1) {
        let frame = CGRect(x: 0, y: 0, width: self.frame.size.width-1, height: borderWidth)
        self.addBorder(frame, color: color, borderWidth: borderWidth)
    }
    
    func addLeftBorder(color: UIColor = UIColor(hex: "#979797"), borderWidth: CGFloat = 1) {
        let frame = CGRect(x: 0, y: 0, width: self.frame.size.width-1, height: borderWidth)
        self.addBorder(frame, color: color, borderWidth: borderWidth)
    }
    
    func addBorder(color: UIColor = UIColor(hex: "#979797"), width: CGFloat = 1) {
        self.layer.borderWidth = width
        self.layer.borderColor = color.cgColor
    }
    
    private func addBorder(_ frame: CGRect, color: UIColor = UIColor(hex: "#979797"), borderWidth: CGFloat = 1) {
        let border = CALayer()
        border.borderColor = color.cgColor
        border.frame = frame
        border.borderWidth = borderWidth
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
    
    
    class func createBarButtonItem(title: String? = nil, imageNamed : String?, style : UIBarButtonItemStyle? = nil, target : AnyObject? = nil, selector : Selector? = nil) -> UIBarButtonItem {
        let button = UIBarButtonItem()
        
        if let theTitle = title {
            button.title = theTitle
        }
        
        if let theImageName = imageNamed {
            button.image = UIImage(named: theImageName)
        }
        
        if let theStyle = style {
            button.style = theStyle
        }
        
        if let theTarget = target {
            button.target = theTarget
        }
        
        if let theSelector = selector {
            button.action = theSelector
        }
        
        return button
    }
}
