//
//  Identifiers.swift
//  BankSummary
//
//  Created by Paul Ortiz on 4/29/18.
//  Copyright © 2018 Paul Ortiz. All rights reserved.
//

import Foundation

enum Identifiers {
    case assets
    case liabitilies
    
    var stringValue: String {
        switch self {
        case .assets: return "assets"
        case .liabitilies: return "liabilities"
        }
    }
}
