//
//  Enums.swift
//  BankSummary
//
//  Created by Paul Ortiz on 4/28/18.
//  Copyright © 2018 Paul Ortiz. All rights reserved.
//

enum ApplicationContext: String {
    case local
    case iam
}
